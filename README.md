# Omni ORM

---
Next gen ORM. Extremely small ~500b

![](https://i.imgur.com/WzJByEu.png)

**WARNING:** It was meant to be used with Typescript. While it is possible to use with plain JS it is not advised and 
will be hard to use.

For Typescript, you need to have these two options turned on in `tsconfig.json`:

```json
{
	"compilerOptions": {
		"emitDecoratorMetadata": true,
		"experimentalDecorators": true
	}
}
```

**Depends on `reflect-metadata`.**

![npm](https://img.shields.io/npm/v/omni-orm)

**WARNING:** API is not yet final

## Requirements

Technically it should have no requirements. Won't run in browser for obvious reasons.

---

## Installation

For yarn:

```
yarn add omni-orm
```

For npm:

```
npm i omni-orm
```

---


## Docs

---

### Imports:

```ts
// ES6 JS/Typescript style
import { OmniORM } from 'omni-orm';

// require
const { OmniORM } = require('omni-orm');
```
