import { getEntityOptions } from './Decorators';
import { ClassType }        from './Interface/ClassType';
import { Adapter }          from './Adapter/Adapter';
import { OmniORM }          from './OmniORM';
import { Repository }       from './Repository/Repository';

export class RepositoryManager<AdapterT extends Adapter> {
	repositories = new Map<any, any>();

	constructor(protected orm: OmniORM<AdapterT>) {
	}

	getRepository<T extends Record<string, any>>(entityClass: ClassType<T>): Repository<T> {
		let repo = this.repositories.get(entityClass);
		if (repo) {
			return repo;
		}

		const options = getEntityOptions(entityClass);

		if (options && options.customRepository) {
			repo = new options.customRepository(entityClass, this.orm);
		}
		else {
			repo = this.orm.adapter.createRepository<T>(entityClass, this.orm);
		}

		this.repositories.set(entityClass, repo);
		return repo;
	}
}