import { LogLevel }          from './Logger/LogLevel';
import { LoggerInterface }   from './Logger/LoggerInterface';
import { RepositoryManager } from './RepositoryManager';
import { Adapter }           from './Adapter/Adapter';
import { EventEmitter }      from 'events';

export class OmniORM<AdapterT extends Adapter> extends EventEmitter {
	manager: RepositoryManager<AdapterT>;
	adapter!: AdapterT;
	loggers: LoggerInterface[] = [];

	constructor() {
		super();
		this.manager = new RepositoryManager(this);
	}

	setAdapter(adapter: AdapterT) {
		this.adapter = adapter;
		this.adapter.setORM(this);
	}

	async connect() {
		const result = await this.adapter.connect();
		if (result !== true) {
			throw result;
		}
	}

	async disconnect() {
		const result = await this.adapter.disconnect();
		if (result !== true) {
			throw result;
		}
	}

	getDialect() {
		return this.adapter.dialect;
	}

	isDialect(dialect: string) {
		return this.getDialect() === dialect;
	}

	addLogger(logger: LoggerInterface) {
		this.loggers.push(logger);
	}

	removeLogger(logger: LoggerInterface) {
		const idx = this.loggers.indexOf(logger);
		if (idx >= 0) {
			this.loggers.splice(idx, 1);
		}
	}

	log(level: LogLevel, type: string, message: string, ...obj: any[]) {
		for (const logger of this.loggers) {
			logger.log(level, type, message, ...obj).catch(console.error);
		}
	}
}