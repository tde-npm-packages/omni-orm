export interface ModelError {
	field: string;
	message: string;
}