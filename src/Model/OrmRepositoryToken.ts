import { InjectorNext }  from './InjectorNext';

export class OrmRepositoryToken {
	constructor(
		public modelClass?: any,
		public ormName: string = 'orm'
	) {
		InjectorNext.init();
	}

	getRepository() {
		const orm = InjectorNext.get(this.ormName);
		if (!orm) {
			throw new Error('Unable to find ORM with name ' + this.ormName);
		}
		return orm.manager.getRepository(this.modelClass);
	}
}