import type { Injector } from 'injector-next';

export class InjectorNext {
	static requested = false;
	static injector: typeof Injector;

	static init() {
		if (InjectorNext.requested) {
			return;
		}
		InjectorNext.requested = true;

		import('injector-next')
			.then(({ Injector }) => {
				InjectorNext.injector = Injector;
			})
			.catch(() => {
				console.log('injector-next not found');
			});
	}

	static get(service: string) {
		return InjectorNext.injector.get(service);
	}
}