import { ModelError } from './ModelError';

export interface ModelInterface {
	/**
	 * Converts object to plain json
	 */
	toJson?(light: boolean, toDatabase: boolean): any;

	fromJson?(data: any, fromDatabase: boolean): void;

	/**
	 * Executed after creating new model
	 */
	afterCreate?(): void;

	/**
	 * Executed after updating new model
	 */
	afterUpdate?(): void;

	/**
	 * Validate if model data is proper
	 */
	validate?(): boolean | ModelError[];
}
