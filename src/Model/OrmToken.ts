import { InjectorNext } from './InjectorNext';

export class OrmToken {

	constructor(
		public ormName: string = 'orm'
	) {
		InjectorNext.init();
	}

	getOrm() {
		return InjectorNext.get(this.ormName);
	}
}