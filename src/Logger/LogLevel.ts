export enum LogLevel {
	Error = 0,
	Warning = 1,
	Info = 2,
	Verbose = 3,
	Debug = 4,
	User = 5
}