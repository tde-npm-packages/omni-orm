import { LogLevel } from './LogLevel';

export interface LoggerInterface {
	log(
		level: LogLevel,
		type: string,
		message: string,
		...obj: any[]
	): Promise<void>;
}