import { ClassType }  from '../Interface/ClassType';
import { OmniORM }    from '../OmniORM';
import { Repository } from '../Repository/Repository';

export interface Adapter {
	dialect: string;
	config: any;
	orm: OmniORM<Adapter>;

	setORM(orm: OmniORM<Adapter>): void;
	connect(): Promise<boolean>;
	disconnect(): Promise<boolean>;
	createRepository<T extends Record<string, any>>(entityClass: ClassType<T>, orm: OmniORM<any>): Repository<T>;
}