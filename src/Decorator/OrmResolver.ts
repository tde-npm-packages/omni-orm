import type { ResolverType }  from 'injector-next';
import { OrmRepositoryToken } from '../Model/OrmRepositoryToken';
import { OrmToken }           from '../Model/OrmToken';

/**
 * Resolver for injector-next
 * @example
 * ```ts
 * Injector.registerResolver(OrmResolver);
 * ```
 */
export const OrmResolver: ResolverType = (token: any) => {
	if (token instanceof OrmRepositoryToken) {
		return token.getRepository();
	}

	if (token instanceof OrmToken) {
		return token.getOrm();
	}

	return null;
};
