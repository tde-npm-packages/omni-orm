import 'reflect-metadata';
import { OrmToken } from '../Model/OrmToken';

export function Orm(ormName?: string | undefined) {
	return (target: any, propertyKey: string | undefined, parameterIndex: number) =>{
		const tokens = Reflect.getMetadata('design:paramtypes', target) || [];

		tokens[parameterIndex] = new OrmToken(ormName);

		// Redefine param types, so we can check where is our namespace suppose to be injected
		Reflect.defineMetadata('design:paramtypes', tokens, target);
	};
}
