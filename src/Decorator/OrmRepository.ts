import 'reflect-metadata';
import { OrmRepositoryToken } from '../Model/OrmRepositoryToken';

export function OrmRepository(modelClass?: any, ormName?: string | undefined) {
	return (target: any, propertyKey: string | undefined, parameterIndex: number) =>{
		const tokens = Reflect.getMetadata('design:paramtypes', target) || [];

		tokens[parameterIndex] = new OrmRepositoryToken(modelClass, ormName);

		// Redefine param types, so we can check where is our namespace suppose to be injected
		Reflect.defineMetadata('design:paramtypes', tokens, target);
	};
}