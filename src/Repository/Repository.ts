import 'reflect-metadata';
import { ClassLike }        from '../Interface/ClassLike';
import { getEntityOptions } from '../Decorators';
import { ClassType }        from '../Interface/ClassType';

export abstract class Repository<T> {
	idField?: string;

	protected transaction: any;
	protected supportsTransaction = false;
	protected entityOptions: any = {};

	protected constructor(
		protected Type: ClassType<T>,
		public orm: any,
		public options?: any
	) {
		this.entityOptions = getEntityOptions(Type);
		this.idField = Reflect.getMetadata('entity:id', this.Type.prototype);
	}

	setTransaction(trx: any) {
		if (!this.supportsTransaction) {
			throw Error('Repository does not support transaction!');
		}
		this.transaction = trx;
	}

	clearTransaction() {
		if (!this.supportsTransaction) {
			throw Error('Repository does not support transaction!');
		}
		this.transaction = null;
	}

	async startTransaction(): Promise<any> {
		throw Error('Method startTransaction not implemented!');
	}

	async commit() {
		throw Error('Method commit not implemented!');
	}

	async rollback() {
		throw Error('Method rollback not implemented!');
	}

	create() {
		return new this.Type();
	}

	abstract insert(entity: T): Promise<T>;

	abstract update(entity: T): Promise<T>;

	abstract updateRaw(data: any, where: any): Promise<void>;

	abstract save(entity: T): Promise<T>;

	abstract remove(entity: T): Promise<this>;

	abstract removeById(id: any): Promise<this>;

	abstract removeMany(query?: ClassLike<T>): Promise<this>;

	abstract find(query?: ClassLike<T>): Promise<T[]>;

	abstract findOne(query: ClassLike<T>): Promise<T | null>;

	abstract findById(id: any): Promise<T | null>;

	abstract findManyById(ids: any[]): Promise<T[]> ;

	abstract findAllIds(): Promise<any[]>;

	abstract count(query?: ClassLike<T>): Promise<number>;

	abstract hydrate(data: any): T | null;
}