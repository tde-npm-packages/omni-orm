import 'reflect-metadata';
import { ClassType } from './Interface/ClassType';

export interface EntityOptions {
	collection?: string;
	table?: string;
	customRepository?: any;
}

export function Entity(options: EntityOptions) {
	return (constructor: any) => {
		Reflect.defineMetadata('entity:options', options, constructor);
	};
}

export function Id() {
	return (target: any, propertyKey: string | symbol) => {
		Reflect.defineMetadata('entity:id', propertyKey, target);
	};
}

export function getEntityOptions<T>(Type: ClassType<T>): EntityOptions {
	return Reflect.getMetadata('entity:options', Type) || {};
}